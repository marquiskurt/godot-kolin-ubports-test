package com.example.helloWorld

import godot.Button
import godot.Label
import godot.Panel
import godot.annotation.RegisterClass
import godot.annotation.RegisterFunction
import godot.annotation.RegisterProperty
import godot.extensions.getNodeAs

@RegisterClass
class HelloWorld: Panel() {

	private lateinit var counterButton: Button
	private lateinit var counterLabel: Label

	@RegisterProperty
	var counter: Int = 0

	@RegisterFunction
	override fun _ready() {
		counterButton = getNodeAs<Button>("VBoxContainer/Button")!!
		counterLabel = getNodeAs<Label>("VBoxContainer/Counter")!!

		counterButton.buttonUp.connect(this, this::onCounterButtonClick)
	}

	@RegisterFunction
	fun onCounterButtonClick() {
		counter += 1
		counterLabel.text = "You clicked me ${counter} times."
	}

}
