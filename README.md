<img src="./click/icon.png" width=64 align="right">

# Godot Kotlin/JVM UBports Test

This repository contains the click package data and files needed to create a click package for Ubuntu Touch for a Godot project using the Kotlin/JVM module.

> ⚠️ This project is highly experimental at the moment, and it currently does not operate correctly on an Ubuntu Touch device. More investigation is needed: https://gitlab.com/abmyii/ubports-godot/-/merge_requests/1#note_656570794

## Build from source

To build the project from source, you'll need the following tools:

- Godot with the Kotlin/JVM module
- OpenJDK 11/Java 11
- Clickable

### Build the Godot project
Clone the repository. In the `hello-world` directory inside of the root, run:

```
$ ./gradlew && /path/to/godot --export-pack "Linux/X11" ../gdkotlin_test.pck
```

You should have a `gdkotlin_test.pck` file located in the root directory of the project.

### Build the click package

Run the following in the terminal to build the click package:

```
$ clickable build
```

The build scripts will automatically download the JDK needed for your chosen architecture, create an embedded JRE with `jlink`, and download a version of Godot with the module installed that supports Ubuntu Touch. The output click file will be located in the new `build` directory.